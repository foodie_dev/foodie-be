import { Food } from "src/food/entities/food.entity";
import { Column, Entity, OneToMany, PrimaryGeneratedColumn } from "typeorm";

@Entity()
export class Menus {
    @PrimaryGeneratedColumn('uuid')
    id: string

    @Column()
    menu: string

    @OneToMany(() => Food, food => food.menu)
    food: Food[]
}