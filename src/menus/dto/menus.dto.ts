import { IsNotEmpty } from "class-validator";

export class MenusDto {
    @IsNotEmpty()
    menu: string
}