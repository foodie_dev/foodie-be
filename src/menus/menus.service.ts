import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Menus } from './entities/menus.entity';
import { Repository } from 'typeorm';
import { MenusDto } from './dto/menus.dto';

@Injectable()
export class MenusService {

    constructor(
        @InjectRepository(Menus)
        private menusRepository: Repository<Menus>
    ) {}

    async createMenus(req: MenusDto) {
        const data = new Menus()

        data.menu = req.menu

        const result = await this.menusRepository.insert(data)

        return {
            data: await this.menusRepository.findOne({
                where: {
                    id: result.identifiers[0].id
                }
            })
        }
    }

    async getAllMenus() {
        return await this.menusRepository.findAndCount({
            relations: {
                food: true
            }
        })
    }

    async getById(id: string) {
        return await this.menusRepository.findOneOrFail({
            where: {
                id: id
            }, 
            relations: {
                food: true
            }
        })
    }

    async delete(id: string) {
        return await this.menusRepository.delete(id)
    }

}
