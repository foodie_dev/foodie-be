import { Body, Controller, Delete, Get, HttpStatus, Param, ParseUUIDPipe, Post } from '@nestjs/common';
import { MenusService } from './menus.service';
import { MenusDto } from './dto/menus.dto';

@Controller('menus')
export class MenusController {

    constructor(
        private menusService: MenusService
    ) {}

    @Post()
    async createMenus(@Body() req: MenusDto) {
        return {
            data: await this.menusService.createMenus(req),
            statusCode: HttpStatus.CREATED,
            message: "Success"
        }
    }

    @Get()
    async findAll() {
        const [data, count] = await this.menusService.getAllMenus()

        return {
            data,
            count,
            statusCode: HttpStatus.OK,
            message: "Success"
        }
    }

    @Get(':id')
    async findOne(@Param('id', ParseUUIDPipe) id: string) {
        return {
            data: await this.menusService.getById(id),
            statusCode: HttpStatus.OK,
            message: 'Success'
        }
    }

    @Delete(':id')
    async delete(@Param('id', ParseUUIDPipe) id: string) {
        await this.menusService.delete(id)

        return {
            statusCode: HttpStatus.OK,
            message: 'Success'
        }
    }

}
