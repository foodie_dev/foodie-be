import { Body, Controller, Delete, Get, HttpStatus, Param, ParseUUIDPipe, Post } from '@nestjs/common';
import { CartService } from './cart.service';
import { CreateCartDto } from './dto/createCart.dto';

@Controller('cart')
export class CartController {
    constructor(
        private cartService: CartService
    ) {}

    @Post()
    async create(@Body() req: CreateCartDto) {
        const result = await this.cartService.createCart(req)

        return {
            data: result,
            statusCode: HttpStatus.CREATED,
            message: 'Success'
        }
    }

    @Get()
    async getAll() {
        const [data, count] = await this.cartService.getAll()

        return {
            data: data,
            count: count,
            statusCode: HttpStatus.OK,
            message: 'Success'
        }
    }

    @Get("user/:idUser")
    async getByIdUser(@Param('idUser', ParseUUIDPipe) idUser: string) {
        return {
            data: await this.cartService.getByIdUser(idUser),
            statusCode: HttpStatus.OK,
            message: "Success"
        }
    }

    @Delete(":id")
    async deleteCart(@Param('id', ParseUUIDPipe) id: string) {
        await this.cartService.deleteCart(id)
        return {
            statusCode: HttpStatus.OK,
            message: "Success"
        }
    }

}
