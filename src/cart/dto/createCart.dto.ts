import { IsNotEmpty } from "class-validator";

export class CreateCartDto {
    @IsNotEmpty()
    message: string

    @IsNotEmpty()
    count: string

    @IsNotEmpty()
    idFood: string

    @IsNotEmpty()
    idUser: string
}