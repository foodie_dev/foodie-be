import { User } from "src/auth/entities/user.entity";
import { Food } from "src/food/entities/food.entity";
import { Column, Entity, ManyToOne, OneToMany, OneToOne, PrimaryGeneratedColumn } from "typeorm";

@Entity()
export class Cart {
    @PrimaryGeneratedColumn('uuid')
    id: string

    @Column()
    message: string

    @Column()
    count: string

    @ManyToOne(() => Food, food => food.cart)
    food: Food

    @ManyToOne(() => User, user => user.cart)
    user: User
}