import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Cart } from './entities/cart.entity';
import { Entity, EntityNotFoundError, Repository } from 'typeorm';
import { Food } from 'src/food/entities/food.entity';
import { CreateCartDto } from './dto/createCart.dto';
import { User } from 'src/auth/entities/user.entity';

@Injectable()
export class CartService {

    constructor(
        @InjectRepository(Cart)
        private cartRepository: Repository<Cart>,

        @InjectRepository(Food)
        private foodRepository: Repository<Food>,

        @InjectRepository(User)
        private userRepository: Repository<User>
    ) {}

    async createCart(req: CreateCartDto) {
        const idFood = await this.foodRepository.findOneOrFail({
            where: {
                id: req.idFood
            }
        })

        const idUser = await this.userRepository.findOneOrFail({
            where: {
                id: req.idUser
            }
        })

        const data = new Cart()
        data.message = req.message
        data.count = req.count
        data.food = idFood
        data.user = idUser

        const result = await this.cartRepository.insert(data)

        return {
            data: await this.cartRepository.findOne({
                where: {
                    id: result.identifiers[0].id
                },
                relations: {
                    food : {
                        menu: true
                    },
                    user: true
                }
            })
        }
    }

    async getAll() {
        return await this.cartRepository.findAndCount({
            relations: {
                food: {
                    menu: true
                },
                user: true
            }
        })
    }

    async getByIdUser(idUser: string) {
        return await this.cartRepository.find({
            where: {
                user: {
                    id: idUser
                }
            },
            relations: {
                food: {
                    menu: true
                },
                user: true
            }
        })
    }

    async deleteCart(id: string) {
        try {
            await this.cartRepository.findOneOrFail({
                where: {
                    id: id
                }
            })
        } catch (error) {
            if (error instanceof EntityNotFoundError) {
                throw new HttpException({
                    statusCode: HttpStatus.NOT_FOUND,
                    message: 'Cart Not Found'
                }, HttpStatus.NOT_FOUND)
            } else {
                throw error
            }
        }

        return this.cartRepository.delete(id)
    }

}
