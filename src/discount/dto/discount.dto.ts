import { IsNotEmpty } from "class-validator";

export class DiscountDto {
    @IsNotEmpty()
    amount: number

    @IsNotEmpty()
    discountPrice: string

    @IsNotEmpty()
    expiredDate: string

    @IsNotEmpty()
    foodId: string
}