import { Food } from "src/food/entities/food.entity";
import { Column, Entity, ManyToOne, OneToMany, OneToOne, PrimaryGeneratedColumn } from "typeorm";

@Entity()
export class Discount {
    @PrimaryGeneratedColumn('uuid')
    id: string

    @Column()
    amount: number

    @Column()
    dicountPrice: string

    @Column()
    expiredDate: string

    @ManyToOne(() => Food, food => food.discount)
    food: Food
}