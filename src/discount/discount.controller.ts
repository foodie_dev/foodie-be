import { Body, Controller, HttpStatus, Post } from '@nestjs/common';
import { DiscountService } from './discount.service';
import { DiscountDto } from './dto/discount.dto';

@Controller('discount')
export class DiscountController {

    constructor(
        private discountService: DiscountService
    ) {}

    @Post()
    async create(@Body() req: DiscountDto) {

        const result = await this.discountService.createDiscount(req)
    
        return {
            data: result,
            statusCode: HttpStatus.CREATED,
            message: 'Success'
        }

    }

}
