import { Module } from '@nestjs/common';
import { DiscountService } from './discount.service';
import { DiscountController } from './discount.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Food } from 'src/food/entities/food.entity';
import { Discount } from './entities/dicount.entity';

@Module({
  imports: [
    TypeOrmModule.forFeature([Food, Discount])
  ],
  providers: [DiscountService],
  controllers: [DiscountController]
})
export class DiscountModule {}
