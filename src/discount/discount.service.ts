import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Discount } from './entities/dicount.entity';
import { Repository } from 'typeorm';
import { Food } from 'src/food/entities/food.entity';
import { DiscountDto } from './dto/discount.dto';

@Injectable()
export class DiscountService {

    constructor (
        @InjectRepository(Discount)
        private discountRepository: Repository<Discount>,

        @InjectRepository(Food)
        private foodRepository: Repository<Food>
    ) {}

    async createDiscount(req: DiscountDto) {
        const foodId = await this.foodRepository.findOneOrFail({
            where: {
                id: req.foodId
            }
        })

        const dicount = (foodId.price * req.amount) / 100

        const data = new Discount();
        data.amount = req.amount
        data.dicountPrice = dicount.toString()
        data.expiredDate = req.expiredDate
        data.food = foodId

        const result = await this.discountRepository.insert(data)

        return await this.discountRepository.findOne({
            where: {
                id: result.identifiers[0].id
            },
            relations: {
                food: true
            }
        })

    }

}
