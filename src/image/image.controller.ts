import { Controller, Post, UseInterceptors, UploadedFile, HttpStatus, Get, Param, Res } from '@nestjs/common';
import { FileInterceptor } from '@nestjs/platform-express';
import * as fs from 'fs'
import { diskStorage } from 'multer';
import { extname } from 'path';
import { of } from 'rxjs'
import { join } from 'path'

export const foodImage = {
    storage: diskStorage({
    destination: './uploads/food', 
    filename: (req, file, cb) => {
        const randomName = Array(32).fill(null).map(() => (Math.round(Math.random() * 16)).toString(16)).join('')
        cb(null, `${randomName}${extname(file.originalname)}`)
      }
    })
  }

@Controller('image')
export class ImageController {
    @Post('/food')
    @UseInterceptors(FileInterceptor('file', foodImage))
    uploadFile(@UploadedFile() file: Express.Multer.File) {
        return {
            data: file.filename,
            statusCode: HttpStatus.CREATED,
            status: 'Success Uplaod'
        }
    }

    @Get('/food/:fileName')
    fileImage(@Param('fileName') fileName: string, @Res() res) {
        return of(res.sendFile(join(process.cwd(), 'uploads/food/' + fileName)))
    }
}
