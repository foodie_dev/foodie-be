import { IsNotEmpty } from "class-validator";

export class CreateFoodDto {
    @IsNotEmpty()
    name: string

    @IsNotEmpty()
    price: number

    @IsNotEmpty()
    stock: string

    @IsNotEmpty()
    img: string

    @IsNotEmpty()
    idMenus: string
}