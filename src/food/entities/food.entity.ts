import { Cart } from "src/cart/entities/cart.entity";
import { Discount } from "src/discount/entities/dicount.entity";
import { Menus } from "src/menus/entities/menus.entity";
import { Column, Entity, ManyToOne, OneToOne, PrimaryGeneratedColumn } from "typeorm";

@Entity()
export class Food {
    @PrimaryGeneratedColumn('uuid')
    id: string

    @Column()
    name: string

    @Column()
    price: number

    @Column()
    stock: string

    @Column()
    img: string

    @ManyToOne(() => Menus, menus => menus.food)
    menu: Menus

    @ManyToOne(() => Cart, cart => cart.food)
    cart: Cart[]

    @OneToOne(() => Discount, discount => discount.food)
    discount: Discount[]
}