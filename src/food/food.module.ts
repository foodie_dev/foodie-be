import { Module } from '@nestjs/common';
import { FoodController } from './food.controller';
import { FoodService } from './food.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Food } from './entities/food.entity';
import { Menus } from 'src/menus/entities/menus.entity';
import { Discount } from 'src/discount/entities/dicount.entity';

@Module({
  imports: [
    TypeOrmModule.forFeature([Food, Menus, Discount])
  ],
  controllers: [FoodController],
  providers: [FoodService]
})
export class FoodModule {}
