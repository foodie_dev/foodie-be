import { Body, Controller, Get, HttpStatus, Post, DefaultValuePipe, ParseIntPipe, ParseUUIDPipe, Param } from '@nestjs/common';
import { FoodService } from './food.service';
import { CreateFoodDto } from './dto/createFood.dto';
import { Query } from '@nestjs/common/decorators';

@Controller('food')
export class FoodController {

    constructor(
        private foodService: FoodService
    ) {}

    @Post()
    async create(@Body() req: CreateFoodDto) {
        const result = await this.foodService.createFood(req)

        return {
            data : result,
            statusCode: HttpStatus.CREATED,
            message: 'Success'
        }
    }

    @Get()
    async paginationAndSearch(
        @Query('page', new DefaultValuePipe(1), ParseIntPipe) page: number,
        @Query('limit', new DefaultValuePipe(1), ParseIntPipe) limit: number,
        @Query('search', new DefaultValuePipe('')) nameFood: string,
        @Query('menu', new DefaultValuePipe('')) menu: string
    ) {
        return this.foodService.paginationAndSearch({page, limit}, nameFood, menu)
    }

    @Get(':id')
    async getById(@Param('id', ParseUUIDPipe) id: string) {
        return {
            data: await this.foodService.getById(id),
            statusCode: HttpStatus.OK,
            message: 'Success'
        }
    }

    @Get('menu/:idMenu') 
    async getFoodByIdMenu(@Param('idMenu', ParseUUIDPipe) id: string) {
        const [data, count] = await this.foodService.getFoodByIdMenu(id)

        return {
            data,
            count,
            statusCode: HttpStatus.OK,
            message: 'Success'
        }
    }

}
