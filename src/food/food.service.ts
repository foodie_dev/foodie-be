import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Food } from './entities/food.entity';
import { Like, Repository } from 'typeorm';
import { Menus } from 'src/menus/entities/menus.entity';
import { CreateFoodDto } from './dto/createFood.dto';
import { Pagination, IPaginationOptions, paginate } from 'nestjs-typeorm-paginate';

@Injectable()
export class FoodService {

    constructor(
        @InjectRepository(Food)
        private foodRepository: Repository<Food>,

        @InjectRepository(Menus)
        private menusRepository: Repository<Menus>
    ) {}

    async createFood(req: CreateFoodDto) {
        const menusId = await this.menusRepository.findOneOrFail({
            where: {
                id: req.idMenus
            }
        })

        const data = new Food();
        data.name = req.name
        data.price = req.price
        data.img = req.img
        data.stock = req.stock
        data.menu = menusId

        const result = await this.foodRepository.insert(data)

        return await this.foodRepository.findOne({
            where: {
                id: result.identifiers[0].id
            },
            relations: {
                menu: true
            }
        })

    }

    async getAll() {
        return await this.foodRepository.findAndCount({
            relations: {
                menu: true
            }
        })
    }

    async paginationAndSearch(options: IPaginationOptions, nameFood: string, menu) : Promise<Pagination<Food>> {
        return paginate<Food>(this.foodRepository, options, {
            where: {
                name: Like(`%${nameFood}%`),
                menu: {
                    menu: menu
                }
            },
            relations: {
                menu: true
            }
        })
    }

    async getById(id: string) {
        return await this.foodRepository.findOneOrFail({
            where: {
                id: id
            },
            relations: {
                menu: true
            }
        })
    }

    async getFoodByIdMenu(id: string) {
        return await this.foodRepository.findAndCount({
            where: {
                menu: {
                    id: id
                }
            }
        })
    }

} 
