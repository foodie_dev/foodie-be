import { Body, Controller, HttpStatus, Post, Get, Delete, Param, ParseUUIDPipe, UseGuards } from '@nestjs/common';
import { AuthService } from './auth.service';
import { RegisterDto } from './dto/register.dto';
import { RoleDto } from './dto/role.dto';
import { LoginDto } from './dto/login.dto';
import { AuthGuard } from './guard/auth.guard';

@Controller('')
export class AuthController {

    constructor(
        private authService: AuthService
    ) {}

    @Post('register')
    async register(@Body() req: RegisterDto) {
        const result = await this.authService.create(req)

        return {
            data: result,
            statusCode: HttpStatus.CREATED,
            message: 'Success'
        }
    }

    @Post('login')
    async signIn(@Body() signInDto: Record<string, any>) {
        return await this.authService.signIn(signInDto.username, signInDto.password);
    }

    @Post('role')
    async createRole(@Body() req: RoleDto) {
        const result = await this.authService.createRole(req)

        return {
            data: result,
            statusCode: HttpStatus.CREATED,
            message: 'Success'
        }
    }

    @Get('users')
    async getAll() {
        const [data, count] = await this.authService.findAll()

        return {
            data: data,
            count: count,
            statusCode : HttpStatus.OK,
            message: 'Success'
        }
    }

    @Get('role')
    async getAllRole() {
        const [data, count] = await this.authService.findAllRole()

        return {
            data: data,
            count: count,
            statusCode : HttpStatus.OK,
            message: 'Success'
        }
    }

    @Get('user/:id')
    async getById(@Param('id', ParseUUIDPipe) id: string) {
        const result = await this.authService.findOne(id)
        
        return {
            data : result,
            statusCode: HttpStatus.OK,
            message: 'Success'
        }
    }

    @Delete('user/:id')
    async delete(@Param('id', ParseUUIDPipe) id: string) {
        await this.authService.deleteUser(id)

        return {
            statusCode: HttpStatus.OK,
            messgae: 'Success'
        }
    }

}
