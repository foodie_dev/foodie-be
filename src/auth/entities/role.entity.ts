import { Column, Entity, OneToMany, PrimaryGeneratedColumn } from "typeorm";
import { User } from "./user.entity";

@Entity()
export class Role {
    @PrimaryGeneratedColumn('uuid')
    id: string

    @Column()
    role: string

    @OneToMany(() => User, user => user.role)
    users: User[]
}