import { Column, CreateDateColumn, DeleteDateColumn, Entity, ManyToOne, PrimaryGeneratedColumn, UpdateDateColumn } from "typeorm";
import { Role } from "./role.entity";
import { Cart } from "src/cart/entities/cart.entity";

@Entity()
export class User {
    @PrimaryGeneratedColumn('uuid')
    id: string

    @Column()
    username: string

    @Column()
    email: string

    @Column()
    password: string

    @Column()
    hp: string

    @Column({default: true})
    isActive: boolean

    @CreateDateColumn({
        type: 'timestamp',
        nullable: false
    })
    createdAt: Date

    @UpdateDateColumn({
        type: 'timestamp',
        nullable: false
    })
    updateAt: Date

    @DeleteDateColumn({
        type: 'timestamp',
        nullable: false
    })
    deletedAt: Date

    @ManyToOne(() => Role, role => role.users)
    role: Role

    @ManyToOne(() => Cart, cart => cart.user)
    cart: Cart[]
}