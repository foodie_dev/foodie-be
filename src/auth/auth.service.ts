import { HttpException, Injectable, UnauthorizedException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { User } from './entities/user.entity';
import { Repository } from 'typeorm';
import { RegisterDto } from './dto/register.dto';
import * as bcrypt from 'bcrypt';
import { Role } from './entities/role.entity';
import { RoleDto } from './dto/role.dto';
import { LoginDto } from './dto/login.dto';
import { JwtService } from '@nestjs/jwt';
import { Roles } from './guard/roles.decorator';
import { RoleEnum } from './guard/role.enum';

@Injectable()
export class AuthService {

    constructor(
        @InjectRepository(User)
        private userRepository: Repository<User>,

        @InjectRepository(Role)
        private roleRepository: Repository<Role>,

        private jwtService: JwtService
    ) {}

    async createRole(req: RoleDto) {
        const data = new Role()

        data.role = req.role

        const result = await this.roleRepository.insert(data)

        return await this.roleRepository.findOne({
            where: {
                id: result.identifiers[0].id
            }
        })
    }

    async create(req: RegisterDto) {

        const hashPassword = await bcrypt.hash(req.password, 12)
        const roleUser = await this.roleRepository.findOneOrFail({
            where: {
                id: 'd8d93dba-de97-4f09-bba0-db88d87b5175'
            }
        })
        
        const data = new User()
        data.username = req.username
        data.email = req.email
        data.hp = req.hp
        data.password = hashPassword
        data.role = roleUser

        const result = await this.userRepository.insert(data)

        return await this.userRepository.findOne({
            where: {
                id: result.identifiers[0].id
            },
            relations: {
                role: true
            }
        })
    }

    async signIn(username: string, pass: string) {
        const user = await this.userRepository.findOneOrFail({
            where: {
                username: username
            },
            relations: {
                role: true
            }
        });
        if (user && await bcrypt.compare(user.password, pass)) {
          throw new UnauthorizedException();
        }
        const { password, ...result } = user;

        const payload = { username: result.username, sub: result.id, email: result.email, role: result.role };

        return {
            access_token: await this.jwtService.signAsync(payload),
        };
    }

    async findAll() {
        return await this.userRepository.findAndCount({
            relations: {
                role: true
            }
        })
    }

    async findAllRole() {
        return await this.roleRepository.findAndCount()
    }

    async findUserByEmail(email: string) {
        return await this.userRepository.findOneOrFail({
            where: {
                email: email
            }
        })
    }

    async findOne(id: string) {
        return await this.userRepository.findOneOrFail({
            where: {
                id
            },
            relations: {
                role: true
            }
        })
    }

    async deleteUser(id: string) {
        return await this.userRepository.delete(id)
    }

}
